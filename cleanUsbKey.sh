#!/bin/bash

# author(s)               : echo <dev@lointaine.fr>
# version                 : 1.0
# repository              :
# Creation date           : 2018/10/30
# Last modification date  :
# Licence                 : GPL v3 https://www.gnu.org/licenses/gpl-3.0.txt
# Subject : nettoyage de clef de mac vers pc

find ./* -name '._*' -delete
